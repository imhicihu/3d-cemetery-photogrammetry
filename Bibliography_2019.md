### 3D
* Mullen, Tony: "*Mastering Blender*", Wiley Publishing, 475pp. ISBN: 9780470407417. (read ---> *done*!)
* Bradley Cantrell & Natalie Yates. *Modeling the environment: techniques and tools for the 3D illustration of dynamic landscapes*. ISBN: 978-0470902943 (read ---> *done*!)
* Simonds, Ben: *Blender Master Class: A Hands-On Guide to Modeling, Sculpting, Materials, and Rendering*. 2013 | 288 Pages | ISBN: 1593274777. (read ---> *done*!)
* Marcus A Magnor (Editor), Christian Theobalt (Editor), Olga Sorkine-Hornung (Editor), Oliver Grau (Editor): *Digital Representations of the Real World: How to Capture, Model, and Render Visual Reality*, CRC Press, (2015), 454 pp.

### Photogrammetry
* Maurer, M.; Rumpler, M.; Wendel, A.; Hoppe, C.; Irschara, A.; Bischof, H.: *Geo-referenced 3d reconstruction: Fusing public geographic data and aerial imagery*. Institute for computer graphics and vision, Graz University of Technology, Austria. (read ---> *done*!)
* Li, X.Q.; Chen, Z.A.; Zhang, L.T.; Jia, D.: *Construction and accuracy test of a 3D model of non-metric camera images using Agisoft Photoscan*. Procedia Environmental Sciences 36 (2016) 184-190. (read ---> *done*!)
* van Oosterom, P., Zlatanova, S., Penninga, F., Fendel, E. (Eds.): *Advances in 3D Geoinformation Systems*, Part III, *Working Group II � Acquisition � Position Paper: Data collection and 3D reconstruction,* Springer Berlin Heidelberg, pp 425-428, 2008, ISBN 978-3-540-72135-2
* Tian, Y.; Gerke, M.; Vosselman, G.; Zhu, Q.: *Knowledge-based building reconstruction from terrestrial video sequences.* ISPRS Journal of Photogrammetry and remote sensing 65 (2010): 395-408.  (read ---> *done*!)
* Tan, T. N.; Sullivan, G. D.;  Baker, K. D.: *[Recovery of Intrinsic and Extrinsic Camera Parameters Using Perspective Views of Rectangle](http://www.bmva.org/bmvc/1995/bmvc-95-017.pdf)*. BMVC, 1995: 177-186.
* Baggio, D.L.: *Mastering opencv with practical computer vision projects*   (for video measurements)
* [Using Vanishing Points for Camera Calibration and Coarse 3D Reconstruction from a Single Image](http://www.irisa.fr/prive/kadi/Reconstruction/paper.ps.gz): E. Guillou, D. Meneveaux, E. Maisel, K. Bouatouch.
* Andreas Georgopoulos and Elisavet Konstantina Stathopoulou: *Data Acquisition for 3D Geometric Recording: State of the Art and Recent Innovations* M.L. Vincent et al. (eds.), Heritage and Archaeology in the DigitalAge, Quantitative Methods in the Humanities and Social Sciences, DOI 10.1007/978-3-319-65370-9_1, ISBN 978-3-319-65370-9, Springer International Publishing AG (2017), 209 pp.
* Criminisi, A.; Reid, I. and A. Zisserman: *[Single View Metrology](https://github.com/jguoaj/single-view-modeling/blob/master/Criminisi99.pdf)*, ICCV 99. 