## Technical requirements ##

* Hardware
     - Macbook 15"
     - 3DConexxion SpaceMouse Pro
     - Apple MagicMouse

* Software
     - 3d software (modelling and handling)
        - [Blender](https://blender.org/)
              - [BlenderGis](https://github.com/domlysz/BlenderGIS): import in Blender most commons GIS data format: Shapefile vector, raster image, geotiff DEM, OpenStreetMap xml
        - [Meshlab](https://github.com/cnr-isti-vclab/meshlab/releases/): mesh edition, correction, conversion & interchange file formats
        - [ReMESH](http://remesh.sourceforge.net/): editor for manifold triangle meshes with advanced repairing features
        - [BakemyScan](http://bakemyscan.org/): easy GUI access to powerful remeshing & retopolgy software, all within blender
        - [CloudCompare](http://www.cloudcompare.org/): 3D point cloud and mesh processing
        - [Instant Meshes](https://github.com/wjakob/instant-meshes): algorithm to implement for an automatic retopology. More data can be found [here](http://igl.ethz.ch/projects/instant-meshes/)
        - [Blender-Addon-Photogrammetry-Importer](https://github.com/SBCV/Blender-Addon-Photogrammetry-Importer): Blender addon to import reconstruction results of several libraries
     - Rendering
        - [Blender Cycles](https://www.cycles-renderer.org/)
        - [Armory Renderer](https://armory3d.org/)
        - [Physically-Based Rendering v1.5.8](https://sketchucation.com/plugin/2101-pbr)
     - Photogrammetry
        - [SuRE](http://www.ifp.uni-stuttgart.de/publications/software/sure/index.en.html): photogrammetry
        - [Blender-photogrammetry](https://github.com/stuarta0/blender-photogrammetry)
        - [Bundler: Structure from Motion (SfM) for Unordered Image Collections](http://www.cs.cornell.edu/~snavely/bundler/)
        - [fSpy importer add-on](https://github.com/stuffmatic/fSpy-Blender): Blender matching camera
     - Video analysis
        - [Tracker](https://physlets.org/tracker/): video analysis and modeling tool
     - Bibliographic searcher:
        - [ScienceFair](http://sciencefair-app.com): Discover, collect, organise, read and analyse scientific papers.
* Data
     - [30-meter resolution elevation data from the Shuttle Radar Topography Mission](https://dwtkns.com/srtm30m/)

## Legal ##

* All trademarks are the property of their respective owners.