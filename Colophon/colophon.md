## Technical requeriments ##

* Hardware
     - Macbook 15"
     - 3DConexxion SpaceMouse Pro
     - Apple MagicMouse

* Software
     - [Blender](https://blender.org/)
     - [Blender Cycles](https://www.cycles-renderer.org/)
     - [Armory Renderer](https://armory3d.org/)
     - [Meshlab](https://github.com/cnr-isti-vclab/meshlab/releases/): mesh edition, correction, conversion & interchange file formats
     - [CloudCompare](http://www.cloudcompare.org/): 3D point cloud and mesh processing
     - [Instant Meshes](https://github.com/wjakob/instant-meshes): algorithm to implement for an automatic retopology. More data can be found [here](http://igl.ethz.ch/projects/instant-meshes/) 
     - [SuRE](http://www.ifp.uni-stuttgart.de/publications/software/sure/index.en.html): photogrammetry
     - [Tracker](https://physlets.org/tracker/): video analysis and modeling tool
     - Bibliographic searcher:
        - [ScienceFair](http://sciencefair-app.com): Discover, collect, organise, read and analyse scientific papers.

## Legal ##

* All trademarks are the property of their respective owners.