## Links
* [Turning 3D Data into Tools for Conservation](http://www.cyark.org/news/turning-3d-data-into-tools-for-conservation)
* [Curated list of 3d reconstruction](https://github.com/openMVG/awesome_3DReconstruction_list)
* [Open Source Photogrammetry](http://opensourcephotogrammetry.blogspot.com/)
* [Photogrammetric Weeks](https://phowo.ifp.uni-stuttgart.de/publications/)