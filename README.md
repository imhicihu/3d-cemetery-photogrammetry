![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)
![issues-open](https://bitbucket.org/repo/ekyaeEE/images/2944199103-issues_open.png)

# RATIONALE #

* This repo shows a swift demonstration between a lot of data in the 2D realm to a 3D transition, through a technique called photogrammetry
![final-final 2016-05-16.png](https://bitbucket.org/repo/8zK5j96/images/1566721667-final-final%202016-05-16.png)

### What is this repository for? ###

* Quick summary
    - Migration through 2D data to 3D, applying photogrametry.
* Version 1.01

### How do I get set up? ###

* Summary of set up
    - _In processing time_
* Configuration
    - _In processing time_
* Dependencies
    - _In processing time_
* Deployment instructions
    - _In processing time_

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/3d-cemetery-photogrammetry/issues?status=new&status=open)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/3d-cemetery-photogrammetry/commits/) section for the current status

### Related repositories ###

* Some repositories linked with this project:
     - [Chapman documentary](https://bitbucket.org/imhicihu/chapman-documentary/src/)
     - [Augmented Reality Cemetery (experimental)](https://bitbucket.org/imhicihu/augmented-reality-cemetery-experimental/src/) 

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/3d-cemetery-photogrammetry/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/d-cemetery-photogrammetry/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### Licence ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png) 