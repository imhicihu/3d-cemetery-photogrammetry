* ~~apply Rationale template to `Readme.md`~~
* append conceptual maps done (extract from Google Drive)
* register an account in https://kuula.co/ for the 3d panorama quicktime photo done years ago
* add renders done (some captions can be found in the [Downloads](https://bitbucket.org/imhicihu/3d-cemetery-photogrammetry/downloads/) section
* upload a section of the cemetery in the Sketchfab account (previously created for the occasion)
* ~~change avatar of this repo~~
* ~~migrate some bibliography to `Bibliography.md` from the `Chapman documentary repo`~~
* ~~Migrate Del.icio.us gathered links to Evernote~~